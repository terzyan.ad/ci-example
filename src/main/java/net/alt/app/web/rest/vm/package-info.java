/**
 * View Models used by Spring MVC REST controllers.
 */
package net.alt.app.web.rest.vm;
